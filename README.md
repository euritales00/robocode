# Projeto Atlas
Ao iniciar esse projeto achei bastante interessante, e desafiador até, a proposta de se programar algo para um "duelo". Nunca tinha programado algo que eu "pudesse ver mexendo", e achei uma oportunidade ótima para poder aprender algo diferente e ir tentando aperfeiçoar o próprio projeto de uma maneira divertida. Fui dormir às 3 da manhã no primeiro dia que comecei. Mas enfim, vamos ao código.

O código do Atlas-2 foi baseado em seu antecessor, Atlas1 que consistia em um Looping de movimentos (frente e trás), com uma verificação 360° ao final de cada um dos movimentos, mas isso demorava o suficiente para que ele fosse alvejado mais vezes que o normal . 

O Atlas-2 vem com a mesma proposta de movimento, entretanto, com um sistema de verificação melhor. Ao invés de rodar 360° ao final de cada movimento, eu dividir a rotação em 4 giros de 90°, onde, ao achar um inimigo, ele cairá num Looping que irá diminuir a área de busca para os 90° onde o  outro competidor foi achado, fazendo que ele demore menos ao parar para a verificação, e também conseguir disparar mais projéteis no inimigo.

## Pontos Fortes e Fracos

A verificação do Scanner com um ângulo mais curto me permitiu ter uma resposta mais rápida dos disparos e uma melhor movimentação para esquivar dos projeteis. fazendo com que ele ganhasse batalhas contra os robôs ja existentes que continham uma movimentação mais complexa, e de miras que focavam com muita precisão.

Apesar de conseguir um ângulo menor,  não tive êxito contra o tank "Walls", que consiste em um movimento em uma só direção ao redor do mapa. Na maioria das vezes, minhas balas não conseguiam atingi-lo , por causa do seu movimento, saindo rapidamente da trajetória da bala e sendo alvejado toda vez que ele passava.

