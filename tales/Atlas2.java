package tales;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Testedois - a robot by (Euritales S.S.)
 */
public class Atlas2 extends Robot
{
	int aux = 0;
	int i;

	public void run() {
		setBodyColor(Color.white);
		setGunColor(Color.black);
		setRadarColor(Color.black);
		setScanColor(Color.white);
	
		while(true) {
		
			ahead(100);
			for(i = 0; i < 4; i++){
				turnGunRight(90);
				if(aux != 0){
					i = 4;
					aux = 0;
				}
			}
			i = 0;
			back(100);
			for(i = 0; i < 4; i++){
				turnGunLeft(90);
				if(aux != 0){
					i = 4;
					aux = 0;
				}
			}
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		
		aux++;
		fire(3);
	}

	public void onHitByBullet(HitByBulletEvent e) {

		turnLeft(70);
	}
	public void onHitWall(HitWallEvent e) {

		turnLeft(90);
		back(70);
	}
}	


